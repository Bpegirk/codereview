﻿using System;
using System.Dynamic;
using System.Net.Http.Headers;

namespace CreditCalculator
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Credit calculator");
            if (args.Length != 3)
            {
                Console.WriteLine("Command to execute:");
                Console.WriteLine("   calc.txt n y p");
                Console.WriteLine("");
                Console.WriteLine("Where: ");
                Console.WriteLine("  n - credit amount");
                Console.WriteLine("  y - credit term in years");
                Console.WriteLine("  p - credit percent");

                return;
            }

            Calculator calc = new Calculator();

            calc.n = Convert.ToDouble(args[0]);
            calc.y = Convert.ToInt32(args[1]);
            calc.p = Convert.ToDouble(args[2]);

            Console.WriteLine("Result:");
            Console.WriteLine("Monthly payments " + Math.Round(calc.getM(), 2));
            Console.WriteLine("Total  payments " + Math.Round(calc.getS(), 2));
        }
    }


    class Calculator
    {
        public double m;
        public double s;

        public double n;
        public int y;
        public double p;

        public Calculator()
        {
            m = 0;
            s = 0;
            n = 0;
            y = 0;
            p = 0;
        }

        public Calculator(float n, int y, int p)
        {
            this.m = 0;
            this.s = 0;

            this.n = n;
            this.y = y;
            this.p = p / 100;
        }


        public double getM()
        {
            double nLocal = this.n;
            int yLocal = this.y;
            double pLocal = this.p;

            this.m = (nLocal * pLocal / 100 * Math.Pow(1 + pLocal / 100, yLocal) / (12 * (Math.Pow(1 + pLocal / 100, y) - 1)));

            return this.m;
        }


        public double getS()
        {
            double nLocal = this.n;
            int yLocal = this.y;
            double pLocal = this.p;

            this.m = (nLocal * pLocal / 100 * Math.Pow(1 + pLocal / 100, yLocal) / (12 * (Math.Pow(1 + pLocal / 100, y) - 1)));

            this.s = this.m * 12 * yLocal;

            return this.s;
        }
    }
}
